# lago

Сервис авторизации
http://id.vedomosti.ru/

## Запуск

Локально запускаем приложение на домене `id.shark.dev` для совместимости cookies. Для этого выполняем:

```
powder link
powder open
```

Файл `.powder` уже содержит правильное название хоста.


## Использование cookie `access_token`

Напрямую получить `access_token` в этом проекте нельзя, так как используется токен от `shark.dev`.

### Для `dev`-окружения:

1. Проверяем, что оба проекта работают с `aquarium` на одном и том же хосте. Проверяем значение `Settings.hosts.aquarium`.
2. Запускаем `http://shark.dev/includes/session/yes`.
3. Убеждаемся, что браузер сохранил `cookie[:access_token]` с доменом `.shark.dev`.
4. Открываем `http://id.shark.dev/`.
5. Убеждаемся, что доступен `access_token`.
6. Можно пользоваться процедурами, которые обращаются к `aquarium`.

### Для `staging`—окружения:

1. Получаем `access_token` на `http://s-shark.vedomosti.ru/`. Кука будет установлена для домена `.s-shark.vedomosti.ru`.
2. Любым способом меняем домен куки на `.vedomosti.ru`.
3. Проверяем, что `access_token` доступен для `s-id.vedomosti.ru`.

## Доступные URL'ы (staging)

* http://s-id.vedomosti.ru/sign_up — регистрация
* http://s-id.vedomosti.ru/sign_in — вход
* http://s-id.vedomosti.ru/sign_out — выход
* http://s-id.vedomosti.ru/user/profile — профайл
* http://s-id.vedomosti.ru/user/profile.json — профайл в JSON
