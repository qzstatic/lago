class Order
  attr_accessor :autopayment, :id, :payment_method, :payment_status, :price

  def initialize(order = {})
    @autopayment, @id, @payment_method, @payment_status, @price = order.values_at(:autopayment, :id, :payment_method, :payment_status, :price)    
  end
  
  def price
    if @price
      Price.new(@price)
    end
  end
end

