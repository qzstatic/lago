class AccessRight
  attr_accessor :start_date, :end_date, :price, :subproduct_title, :subproduct_period, :payment_id
  
  def initialize(hash)
    hash.each do |key, value|
      define_singleton_method(key) { value }
    end
  end
end
