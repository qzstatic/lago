class Price
  attr_accessor :id, :product, :period, :value

  def initialize(price = {})
    @id, @product, @period, @value = price.values_at(:id, :product, :period, :value)
  end
end
