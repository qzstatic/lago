class User
  include ActiveModel::Model
  
  attr_accessor :id, :status, :avatar, :birthday, :address
  attr_accessor :company_speciality_id, :appointment_id, :income_id, :auto_id
  attr_accessor :access_token, :access_rights, :autopay, :created_at, :sub_end_date, :renewal_date
  
  attr_accessor :ip, :mail_types
  
  validates_presence_of :first_name, :last_name, :email
  validates_acceptance_of :rules, message: 'надо согласиться'
  
  %i(first_name last_name phone email nickname company).each do |attribute|
    attr_reader attribute
    
    define_method("#{attribute}=") do |value|
      instance_variable_set("@#{attribute}", value.to_s.strip)
    end
  end
  
  def attributes
    {
      first_name:   first_name,
      last_name:    last_name,
      email:        email,
      phone:        phone,
      nickname:     nickname,
      access_token: access_token, 
      mail_types:   mail_types
    }
  end
  
  def access_rights
    @access_rights.map { |ar| AccessRight.new(ar) }
  end
  
  def save
    if valid?
      response = self.class.client.post(ip, self.class.sign_up_url, user: attributes)
      
      if response.success?
        true
      else
        @aquarium_errors = response.body
        false
      end
    end
  end
  
  def error_messages
    if valid?
      @aquarium_errors
    else
      errors.messages
    end
  end
  
  def avatar_url
    if avatar.present? && avatar.key?(:versions) && avatar[:versions].key?(:original)
      "http://#{Settings.hosts.agami}#{avatar[:versions][:original][:url]}"
    end
  end
  
  # for hypercomments
  def for_auth_hash
    {
      nick:        nickname,
      avatar:      avatar_url,
      id:          id.to_s,
      email:       email,
      profile_url: "#{Settings.hosts.shark}/user/profile"
    }
  end
  
  # for hypercomments
  def auth_hash
    timestamp   = Time.now().to_i
    user_base64 = Base64.encode64(for_auth_hash.to_json.to_s).split.join
    digest      = Digest::MD5.hexdigest("#{Settings.hypercomments.secret_key}#{user_base64}#{timestamp}")
    "#{user_base64}_#{timestamp}_#{digest}" if status=='subscriber'
  end
  
  # for falco
  def for_user_options
    {
      id:            id,
      status:        status,
      auth_provider: provider,
      created_at:    created_at
    }.to_json
  end

  def for_mailing
    {
      mail_types: mail_types
    }.to_json
  end
  
  def provider
    response = self.class.client.get(ip, "/shark/sessions/#{access_token}")
    if response.success?
      response.body[:account_type]
    end
  end
  
  class << self
    attr_reader :options
    
    def sign_up_url
      raise NotImplementedError
    end
    
    def client
      @client ||= AquariumClient.new
    end
    
    def confirm(token, ip)
      client.post(ip, '/shark/confirm', token: token).success?
    end
  end
end
