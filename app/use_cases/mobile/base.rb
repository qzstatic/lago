module Mobile
  class Base

    attr_reader :errors, :redirect

    def initialize
    end

    def call
    end

    def errors
    end

    def client
      @client ||= AquariumClient.new
    end
  end
end