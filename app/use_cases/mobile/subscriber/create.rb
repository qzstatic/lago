module Mobile
  module Subscriber
    class Create < Base

      def initialize(ip, email, access_token)
        @ip = ip
        @email = email
        @access_token = access_token
        @errors = nil
        @redirect = nil
      end

      def call
        response = client.post(@ip, "/lago/mobile_users/#{@access_token}/subscribe", {
            attributes: {
                email: @email,
                mailing_types: 'newsrelease'
            }
        }, @access_token)
        if !response.success? || response.body.key?(:errors)
          @errors = response.body[:errors]
        elsif response.success? && response.body.key?(:redirect)
          @redirect = response.body[:redirect]
        end
        @redirect
      end

      def redirect
        @redirect
      end

      def errors
        @errors
      end
    end
  end
end