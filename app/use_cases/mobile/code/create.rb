module Mobile
  module Code
    class Create < Base

      attr_reader :phone, :errors

      def initialize(ip, msisdn, access_token, return_url=nil)
        @msisdn = msisdn
        @access_token = access_token
        @errors = nil
        @return_url = return_url
        @ip = ip
      end


      def call
        response = client.post(@ip, "/lago/mobile_users/#{@access_token}/codes",
                               {
                                   attributes: {
                                       msisdn: @msisdn,
                                       return_url: @return_url
                                   }
                               },
                               @access_token
        )
        if response.success?
          @redirect = response.body[:redirect]
        else
          @errors = response.body[:errors]
        end
        !@errors
      end
    end
  end
end