module Mobile
  module Code
    class Check < Base

      attr_reader :otp_token, :code, :msisdn

      def initialize(code, access_token, ip)
        @code = code
        @access_token = access_token
        @errors = nil
        @ip = ip
      end

      def call
        response = client.post(@ip, "/lago/mobile_users/#{@access_token}/codes/check", {
            attributes: { code: @code }
        }, @access_token)
        if response.success?
          @redirect = response.body[:redirect]
        else
          @msisdn = response.body[:msisdn]
          @errors = response.body[:errors]
        end
        !@errors
      end

      def errors
        @errors
      end
    end
  end
end