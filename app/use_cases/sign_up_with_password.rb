class SignUpWithPassword
  extend Forwardable
  
  def_delegators :form, :success?, :user_data
  
  attr_reader :form, :new_token
  private :form
  
  def initialize(form)
    @form = form
  end
  
  def call
    form.save
  rescue AquariumClient::Unauthorized
    set_cookie
    form.save
  end
  
private
  def set_cookie
    response = client.post(form.ip, '/shark/sessions', session: { ip: form.ip })
    
    if response.success?
      @new_token = response.body[:access_token]
      form.access_token = @new_token
    end
  end
  
  def client
    @client ||= AquariumClient.new
  end
end
