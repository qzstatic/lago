class SignInWithOauth
  attr_reader :form, :status, :new_token
  
  def initialize(form)
    @form = form
  end
  
  def call
    send_to_aquarium
  rescue AquariumClient::Unauthorized
    set_cookie
    send_to_aquarium
  end
  
  def success?
    status == 200
  end
  
  def blocked?
    status == 403
  end
  
  def linked?
    use_case = CreateSocialAccountLink.new(form)
    use_case.call
    use_case.success?
  end
  
private
  def send_to_aquarium
    data = {
      provider:     form.provider,
      external_id:  form.external_id,
      access_token: form.access_token
    }
    
    if form.provider == 'facebook'
      old_id = Shark::FacebookIdConvertor.old_id_for(form.external_id)
      data[:old_external_id] = old_id unless old_id.nil?
    end
    
    response = client.post(form.ip, '/shark/sign_in_with_oauth', data)
    @status = response.status
    
    dump_to_redis unless success? || blocked?
  end
  
  def dump_to_redis
    key = "oauth:#{form.access_token}"
    value = Oj.dump(provider: form.provider, external_id: form.external_id)
    
    Shark::Redis.connection.set(key, value)
    Shark::Redis.connection.expire(key, 10.minutes)
  end
  
  def set_cookie
    response = client.post(form.ip, '/shark/sessions', session: { ip: form.ip })
    
    if response.success?
      @new_token = response.body[:access_token]
      form.access_token = @new_token
    end
  end
  
  def client
    @client ||= AquariumClient.new
  end
end
