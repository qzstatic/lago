class SocialAccountLinkConfirm
  attr_reader :confirmation_token, :access_token, :ip
  
  def initialize(confirmation_token, access_token, ip)
    @confirmation_token = confirmation_token
    @access_token = access_token
    @ip = ip
  end
  
  def call
    response = aquarium_client.post(
      ip,
      '/shark/social_account_link_confirm',
      confirmation_token: confirmation_token,
      access_token: access_token
    )
    
    @success = response.success?
  end
  
  def success?
    @success
  end
  
private
  def aquarium_client
    @aquarium_client ||= AquariumClient.new
  end
end
