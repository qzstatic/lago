class SignUpWithOauth
  extend Forwardable
  
  def_delegators :form, :success?
  
  attr_reader :form
  
  def initialize(form)
    @form = form
    assign_data_from_redis
  end
  
  def call
    form.save
    remove_data_from_redis if success?
  end
  
private
  def assign_data_from_redis
    unless provider_data.nil?
      form.provider    = provider_data[:provider]
      form.external_id = provider_data[:external_id]
    end
  end
  
  def provider_data
    @provider_data ||= begin
      json = Shark::Redis.connection.get("oauth:#{form.access_token}")
      Oj.load(json) unless json.nil?
    end
  end
  
  def remove_data_from_redis
    Shark::Redis.connection.del("oauth:#{form.access_token}")
  end
end
