class CheckPasswordResetToken
  attr_reader :ip, :password_reset_token
  
  def initialize(password_reset_token, ip)
    @password_reset_token = password_reset_token
    @ip = ip
  end
  
  def call
    response = aquarium_client.get(
      ip,
      '/shark/check_password_reset_token',
      password_reset_token: password_reset_token
    )
    @success = response.success?
  end
  
  def success?
    @success
  end

private
  def aquarium_client
    @aquarium_client ||= AquariumClient.new
  end
end
