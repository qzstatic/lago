class CreateContact
  attr_reader :email, :type_slug, :ip
  
  def initialize(email, type_slug, ip)
    @email = email
    @type_slug = type_slug
    @ip = ip
  end
  
  def call
    @response = aquarium_client.post(ip, '/shark/contacts', contact: { email: email }, type_slugs: [type_slug])
  end
  
  def success?
    @response && @response.success?
  end

  def contact
    @response && Hashie::Mash.new(@response.body[:contact])
  end

  def mail_type
    @response && Hashie::Mash.new(@response.body[:mail_types].first)
  end

  def errors
    @response && @response.body[:errors] || []
  end
  
private
  def aquarium_client
    @aquarium_client ||= AquariumClient.new
  end
end
