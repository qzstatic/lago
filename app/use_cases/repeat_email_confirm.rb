class RepeatEmailConfirm
  attr_reader :ip, :access_token
  
  def initialize(access_token, ip)
    @access_token = access_token
    @ip = ip
    @success = false
  end
  
  def call
    response = aquarium_client.put(ip, "/shark/users/#{access_token}/repeat_email_confirm")
    @success = response.success?
  end
  
  def success?
    @success
  end
  
private
  def aquarium_client
    @aquarium_client ||= AquariumClient.new
  end
end
