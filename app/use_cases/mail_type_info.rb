class MailTypeInfo
  attr_reader :slug, :ip
  
  def initialize(slug, ip)
    @slug = slug
    @ip = ip
  end
  
  def call
    @response = aquarium_client.get(ip, "/shark/mailing/mail_types/#{slug}")
    @response.success?
  end
  
  def success?
    @response && @response.success
  end

  def mail_type
    @response && Hashie::Mash.new(@response.body)
  end
  
private
  def aquarium_client
    @aquarium_client ||= AquariumClient.new
  end
end
