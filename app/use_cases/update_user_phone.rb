class UpdateUserPhone
  attr_reader :ip, :access_token, :phone
  
  def initialize(ip, access_token, phone)
    @phone = phone
    @access_token = access_token
    @ip = ip
  end
  
  def call
    response = client.patch(
      ip,
      profile_path,
      user: { phone: phone }
    )
  
    @success = response.success?
  end
  
  def success?
    @success
  end
  
private
  def profile_path
    "/shark/users/#{access_token}"
  end
  
  def client
    @client ||= AquariumClient.new
  end
end
