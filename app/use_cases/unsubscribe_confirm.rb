class UnsubscribeConfirm
  attr_reader :ip, :token, :response
  
  def initialize(token, ip)
    @token = token
    @ip = ip
  end
  
  def call
    @response = aquarium_client.post(ip, '/shark/contacts/unsubscribe', unsubscribe_token: token)
  end
  
  def success?
    response && response.success?
  end

  def contact
    Hashie::Mash.new(response.body[:contact]) if success?
  end

  def mail_type
    Hashie::Mash.new(response.body[:mail_type]) if success?
  end
  
private
  def aquarium_client
    @aquarium_client ||= AquariumClient.new
  end
end
