class CreateSocialAccountLink
  attr_reader :form
  
  def initialize(form)
    @form = form
  end
  
  def call
    response = aquarium_client.post(
      form.ip,
      '/shark/create_social_account_link',
      email: form.email,
      provider: form.provider,
      external_id: form.external_id,
      access_token: form.access_token
    )
    @success = response.success?
  end
  
  def success?
    @success
  end

private
  def aquarium_client
    @aquarium_client ||= AquariumClient.new
  end
end
