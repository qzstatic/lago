module ApplicationHelper
  def render_title(title)
    content_for :title, "id.vedomosti.ru — #{title}"
  end
  def has_active_subscription(access_rights)
    access_rights.any?{|a| Time.parse(a[:end_date]) && Time.parse(a[:end_date]) > Time.now && a[:paymethod]  }
    #   && a[:paymethod][:slug] == 'card'
  end
  def active_subscription(access_rights)
    access_rights.find{|a| Time.parse(a[:end_date]) && Time.parse(a[:end_date]) > Time.now && a[:paymethod]  }
  end

  def render_block(path)
    if Rails.env.development?
      ssi_replacements = {
          '$owl_state'           => Settings.owl.state
      }
      path.gsub!(/\$[a-zA-Z_]+/, ssi_replacements)

      full_url                = "http://#{request.host}:#{request.port}#{path}"
      mock_env                = Rack::MockRequest.env_for(full_url).merge('REMOTE_ADDR' => request.ip)
      mock_env["HTTP_COOKIE"] = "access_token=#{cookies[:access_token]}"

      if (body = Rails.application.call(mock_env).last).respond_to?(:join)
        raw(body.join)
      end
    else
      raw %Q(<!--# include virtual="#{path}" -->)
    end
  end
end
