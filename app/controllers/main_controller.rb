class MainController < ApplicationController
  before_action :redirect_if_authorized!, only: %i(index)

  def index
  end
  
  def debug
    render layout: 'empty'
  end

  def files
    render partial: 'assets', layout: false
  end
  
  def auth
    set_return_to_cookie unless return_to
  end
end
