class ApplicationController < ActionController::Base
  before_action :add_assets_links

  layout :set_layout

  def not_found
    render 'errors/not_found', status: :not_found, layout: 'error'
  end

  private
  helper_method :access_token

  def access_token
    request.headers['X-Access-Token'] || cookies[:access_token]
  end

  def return_to
    cookies[:return_to]
  end

  def user(access_token)
    return if access_token.nil? || access_token.empty?
    response  = client.get(request.ip, "/shark/users/#{access_token}", {}, access_token)
    user_data = { ip: request.ip, access_token: access_token }

    User.new(response.body.merge(user_data)) if response.success?
  end

  def set_cookie(name, value, expiration_date = nil)
    ::Rack::Utils.set_cookie_header!(
        response.headers,
        name,
        value:   value,
        expires: expiration_date,
        domain:  Settings.cookie_domain,
        path:    '/'
    )
  end

  def unset_cookie(name)
    ::Rack::Utils.set_cookie_header!(
        response.headers,
        name,
        expires: Time.at(0),
        domain:  Settings.cookie_domain,
        path:    '/'
    )
  end

  def token_cookie_header(access_token)
    {
        access_token: access_token,
        expires:      20.years.from_now.to_s(:rfc822),
        domain:       Settings.cookie_domain,
        path:         '/'
    }.map { |key, value| "#{key}=#{value}" }.join('; ')
  end

  def delete_cookie_and_redirect(fallback = root_url)
    unset_cookie(:return_to) if return_to
    redirect_to cookies[:return_to] || fallback
  end

  def set_return_to_cookie(url = request.referer)
    set_cookie(:return_to, url) if url.present?
  end

  def client
    AquariumClient.new
  end

  def add_assets_links
    @assets = view_context.render(partial: 'assets')
  end

  def layout?
    params.fetch(:layout, false)
  end

  def render_success
    if layout?
      delete_cookie_and_redirect
    else
      render partial: 'refresh'
    end
  end

  def set_layout
    layout? ? 'application' : false
  end

  def return_url
    @return_url ||= (params[:returnUrl] || session[:return_url] || return_to || Settings.hosts.shark).to_s
  end

  def operator
    @operator ||= params.require(:operator)
  end

  def owl_state
    request.headers['X-State']
  end

  def redirect_if_authorized!
    set_return_to_cookie unless return_to
    begin
      redirect_to users_show_url if user(access_token)
    rescue AquariumClient::Unauthorized
      nil
    end
  end
end
