class RegisterController < ApplicationController
  before_action :redirect_if_authorized!, only: %i(sign_up_form)
  helper_method :return_url

  def sign_up_form
    if layout?
      @form = :sign_up
      render 'main/index' and return
    end
    @registration_form = PasswordRegistrationForm.new
  end

  def sign_up_with_password
    @registration_form = PasswordRegistrationForm.new(
      access_token: access_token,
      ip: request.ip,
      **params.fetch(:password_registration_form, {}).symbolize_keys
    )

    use_case = SignUpWithPassword.new(@registration_form)
    use_case.call

    if use_case.new_token.present?
      response.headers['Set-Cookie'] = token_cookie_header(use_case.new_token)
    end

    if use_case.success?
      respond_to do |format|

        # Для внедрённых форм делаем редирект через js, для остальных — обычный редирект.
        response.headers['registered'] = 'true'
        format.html { layout? ? redirect_to('/user/profile') : render(partial: 'refresh') }
        format.json { render json: use_case.user_data }
      end
    else
      response.headers['registered'] = 'false'
      respond_to do |format|
        format.html { render 'register/sign_up_form' }
        format.json { render json: @registration_form.errors, status: :unprocessable_entity }
      end
    end
  end

  def oauth_callback
    @form = OauthRegistrationForm.from_auth_hash(
      auth_hash,
      ip: request.ip,
      access_token: access_token
    )

    use_case = SignInWithOauth.new(@form)
    use_case.call

    if use_case.new_token.present?
      response.headers['Set-Cookie'] = token_cookie_header(use_case.new_token)
    end

    if use_case.success?
      delete_cookie_and_redirect
    elsif use_case.linked?
      redirect_to linked_account_url
    elsif use_case.blocked?
      redirect_to blocked_user_url
    else
      respond_to do |format|
        format.html
      end
    end
  end

  def sign_up_with_oauth
    @form = OauthRegistrationForm.new(
      access_token: access_token,
      ip: request.ip,
      **params[:oauth_registration_form].symbolize_keys
    )

    use_case = SignUpWithOauth.new(@form)
    use_case.call

    if use_case.success?
      delete_cookie_and_redirect
    else
      respond_to do |format|
        format.html { render 'register/oauth_callback' }
      end
    end
  end

  def oauth_failure
    render text: 'failure'
  end

  def email_form
    @email_form = EmailForm.new
  end

  def check_email
    @email_form = EmailForm.new(params[:email_form])

    if @email_form.valid?
      use_case = SendPasswordResetEmail.new(params[:email_form][:email], request.ip)
      use_case.call
      @success = use_case.success?
      @email_form.errors.add(:email, :not_found) unless @success
    end

    render 'register/email_form'
  end

  def password_form
    use_case = CheckPasswordResetToken.new(params[:password_reset_token], request.ip)
    use_case.call

    @password_form = PasswordForm.new
    @check_token_success = use_case.success?
  end

  def password_update
    @check_token_success = true

    @password_form = PasswordForm.new(params[:password_form])

    if @password_form.valid?
      use_case = ResetPassword.new(
        params[:password_form][:password],
        params[:password_reset_token],
        access_token,
        request.ip
      )
      use_case.call

      @success = use_case.success?
      @password_form.errors.add(:password, :unknown_error) unless @success
    end

    render 'register/password_form'
  end

  def confirm
    if User.confirm(params[:token], request.ip)
      delete_cookie_and_redirect #, notice: 'ok'
    else
      redirect_to users_show_url, notice: 'fail'
    end
  end

  def social_account_link_confirm
    use_case = SocialAccountLinkConfirm.new(params[:token], access_token, request.ip)
    use_case.call

    if @success = use_case.success?
      render 'social_account_link_success'
    else
      render 'social_account_link_failed'
    end
  end

private
  def auth_hash
    request.env['omniauth.auth']
  end
end
