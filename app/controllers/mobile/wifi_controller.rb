class Mobile::WifiController < Mobile::MobileController
  before_action :set_return_url!, only: %i(new for_user subscribe)

  include GelfExt::Controller::Logger

  def subscribe
    @operator = params[:operator]
  end

  def show
  end
end
