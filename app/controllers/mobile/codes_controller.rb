class Mobile::CodesController < Mobile::MobileController

  include GelfExt::Controller::Logger

  #GET /mobile/codes/new
  def new
  end

  #POST /mobile/codes/create
  def create
    use_case = Mobile::Code::Create.new(ip, msisdn, access_token, return_url)
    @success = use_case.call
    if @success
      redirect_to use_case.redirect
    else
      @error = use_case.errors.first
      render 'new'
    end
  end

  def confirm
    msisdn
  end

  #POST /mobile/codes/check
  def check
    use_case = Mobile::Code::Check.new(code, access_token, ip)
    @success = use_case.call
    if @success
      redirect_to use_case.redirect
    else
      @msisdn = use_case.msisdn
      @errors = use_case.errors
      @code = code
      render 'confirm'
    end
  end

  private

  def msisdn
    @msisdn ||= "#{params[:phone].scan(/\d+/).join}"
  end

  def code
    params[:code]
  end
end
