class Mobile::MobileController < ApplicationController
  helper_method :operator, :owl_state, :return_url

  include GelfExt::Controller::Logger

  layout 'mobile/application'

  def new
    case operator
    when 'mts'
      redirect_to [Settings.hosts.metynnis, 'paywall_mobile', "#{operator}?returnUrl=#{return_url}&owl_state=#{owl_state}"].join('/')
    else
      set_return_url!
      @return_url = return_url
      @operator = operator
      render 'mobile/new'
    end
  end

  private

  def set_return_url!
    set_return_to_cookie(return_url)
    session[:return_url] = return_url
  end

  def authorize_mobile_user!
    if !current_user
      head :unauthorized
    end
  end

  def ip
    request.ip
  end

  def current_user
    response = client.get(ip, "/lago/mobile_users/#{access_token}", {}, access_token)
    if response.success? && !response.body.key(:status) && !response.body[:status].eql?('error')
      OpenStruct.new(response.body)
    else
      nil
    end
  end
end
