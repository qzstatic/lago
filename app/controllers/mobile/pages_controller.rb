class Mobile::PagesController < ApplicationController
  helper_method :operator, :owl_state, :return_url, :msisdn

  layout 'mobile/application'

  include GelfExt::Controller::Logger
  def wait
    @operator = params[:operator]
    @subscription_id = params[:subscription_id]
  end

  private

  def msisdn
    @msisdn ||= request.headers['HTTP_X_MSISDN']
  end
end
