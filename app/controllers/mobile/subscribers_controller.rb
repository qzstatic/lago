class Mobile::SubscribersController < Mobile::MobileController

  include GelfExt::Controller::Logger

   before_action :authorize_mobile_user!

  def new
    @return_url = current_user.return_url
  end

  def create
    use_case = Mobile::Subscriber::Create.new(ip, email, access_token)
    @success = use_case.call
    if @success
      redirect_to use_case.redirect and return
    else
      @errors = use_case.errors
      render 'new'
    end
  end

  private

  def email
    params[:email]
  end
end
