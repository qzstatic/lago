class UsersController < ApplicationController
  before_action :set_current_user

  def show
    if @user_data = user_profile(access_token)
      @form = ProfileForm.new(access_token: access_token, **@user_data)
      @password_updated = params[:password_updated]
      @access_rights = access_rights_data(@form.access_rights)
      
      respond_to do |format|
        format.html
        format.json { render json: { user: @user_data, access_rights: @access_rights } }
      end
    else
      redirect_to root_url
    end
  end

  def update
    if @user_data = user_profile(access_token)
      profile_form = params[:profile_form].symbolize_keys
      @form = ProfileForm.new(ip: request.ip, access_token: access_token, **profile_form)
      if @form.save
        # страшный костыль, спасибо проектантам
        # сохраняем всю форму, а нужно поймать только признак сохранения пароля
        if profile_form[:password].present?
          redirect_to users_show_path(password_updated: true)
        else
          redirect_to users_show_path
        end
      else
        # если обнаружили ошибку заменим mail_types на нормальные объекты (а то там остаются введенный массив id-шников)
        # TODO: По хорошему их надо развести по разным переменным
        @user_data[:mail_types].each { |t| t[:checked] = Array(profile_form[:mail_types]).include?(t[:id].to_s) ? true : false }
        @form = ProfileForm.new(ip: request.ip, access_token: access_token, **profile_form.merge(mail_types: @user_data[:mail_types]))
        render :show
      end
    else
      redirect_to root_url
    end
  end

  def update_phone
    use_case = UpdateUserPhone.new(request.ip, access_token, params[:phone])
    use_case.call
    
    if use_case.success?
      head :ok
    else
      head :unprocessable_entity
    end
  end
  
  def repeat_email_confirm
    use_case = RepeatEmailConfirm.new(access_token, request.ip)
    use_case.call
    
    if use_case.success?
      head :ok
    else
      head :unprocessable_entity
    end
  end

  # возвращает юзера для buy.vedomosti.ru
  def buy_profile
    @user = user_profile(access_token)
    @form = ProfileForm.new(ip: request.ip, access_token: access_token)

    respond_to do |format|
      format.html
      format.json { render json: { user: @user, form: @form, access_rights: @access_rights } }
    end
  end


private
  def user_profile(access_token)
    response = client.get(request.ip, "/shark/users/#{access_token}/profile", {}, access_token)
    response.body if response.success?
  end

  def set_current_user
    @current_user = user(access_token)
  end

  def access_rights_data(access_rights)
    access_rights.map do |access_right|
      data = nil
      
      if access_right.start_date.present? && access_right.end_date.present?
        data = {
          start_date: I18n.l(Time.parse(access_right.start_date), format: :date_only),
          end_date:   I18n.l(Time.parse(access_right.end_date), format: :date_only)
        }
        
        data[:price]   = access_right.price
        data[:product] = access_right.subproduct_title
        data[:period]  = access_right.subproduct_period
        data[:payment_id]  = access_right.payment_id
        data[:paymethod] = access_right.paymethod
        data
      end
    end.compact
  end
end
