class ContactsController < ApplicationController
  def subscribe_form
    use_case = MailTypeInfo.new(params.require(:slug), request.ip)
    if use_case.call
      @mail_type = use_case.mail_type
      @subscribe_form = SubscribeForm.new(type_slug: use_case.slug)
      respond_to do |format|
        format.html
        format.json { render json: { mail_type: @mail_type, subscribe_form: @subscribe_form } }
      end
    else
      not_found
    end
  end

  def subscribe
    @subscribe_form = SubscribeForm.new(params[:subscribe_form])
    if @subscribe_form.valid?
      use_case = CreateContact.new(params[:subscribe_form][:email], params[:subscribe_form][:type_slug], request.ip)
      use_case.call
      @success = use_case.success?
      @mail_type = use_case.mail_type
      @contact = use_case.contact
      unless @success
        @subscribe_form.errors.add(:email, :already_subscribed) if use_case.errors.include?('already subscribed')
        @subscribe_form.errors.add(:type_slug, :invalid_type) if use_case.errors.include?('invalid mailing type(s)')
      end
    else
      use_case = MailTypeInfo.new(params[:subscribe_form][:type_slug], request.ip)
      unless use_case.call
        not_found
        return
      end
      @mail_type = use_case.mail_type
    end
    render 'contacts/subscribe_form'
  end

  def subscribe_ajax
    use_case = CreateContact.new(params.require(:email), params.require(:slug), request.ip)
    use_case.call
    if use_case.success?
      render json: {
        mail_type: use_case.mail_type,
        contact:   use_case.contact
      }
    else
      render json: {
        mail_type: use_case.mail_type,
        contact:   use_case.contact,
        errors:     use_case.errors
      }, status: :unprocessable_entity
    end
  end

  def subscribe_confirm
    use_case = SubscribeConfirm.new(params[:token], request.ip)
    use_case.call

    if @success = use_case.success?
      @contact   = use_case.contact
      @mail_type = use_case.mail_type
      respond_to do |format|
        format.html
        format.json { render json: { contact: @contact, mail_type: @mail_type } }
      end
    else
      not_found
    end
  end

  def unsubscribe
    @token = params.require(:token)
    use_case = UnsubscribeConfirm.new(@token, request.ip)
    use_case.call

    if use_case.success?
      @contact   = use_case.contact
      @mail_type = use_case.mail_type
      respond_to do |format|
        format.html
        format.json { render json: { token: @token, contact: @contact, mail_type: @mail_type } }
      end
    else
      not_found
    end
  end
end
