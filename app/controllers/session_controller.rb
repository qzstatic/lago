class SessionController < ApplicationController
  before_action :redirect_if_authorized!, only: %i(sign_in_form)
  helper_method :return_url

  def sign_in_form
    if layout?
      @form = :sign_in
      render 'main/index' and return
    end
    @sign_in_form = SignInForm.new
  end

  def sign_in_with_password
    @sign_in_form = SignInForm.new(
        access_token: access_token,
        ip: request.ip,
        **params.fetch(:sign_in_form, {}).symbolize_keys
    )

    use_case = SignInWithPassword.new(@sign_in_form)
    use_case.call

    if use_case.new_token.present?
      response.headers['Set-Cookie'] = token_cookie_header(use_case.new_token)
    end

    if use_case.success?
      unset_cookie(:return_to)
      respond_to do |format|
        format.html { render partial: 'refresh' }
        format.json { render json: use_case.user_data }
      end
    elsif use_case.blocked?
        render nothing: true, status: :unprocessable_entity, :location => blocked_user_url
    else
      respond_to do |format|
        format.html   { render 'session/sign_in_form' }
        format.json   { head :unprocessable_entity }
      end
    end
  end

  def sign_out
    client.delete(
        request.ip,
        '/shark/sign_out',
        access_token: access_token
    )
    redirect_to request.referer || root_url
  end

  def user_profile
    @user = user(params[:access_token]) if params.key?(:access_token)

    respond_to do |format|
      format.html
      format.json { render json: @user }
    end
  end
end
