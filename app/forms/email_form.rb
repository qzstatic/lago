class EmailForm
  include Virtus.model
  include ActiveModel::Conversion
  include ActiveModel::Validations

  attribute :email, String
  
  validates :email, presence: true, email: true
  
  def email=(new_email)
    super(new_email.strip.downcase)
  end
end
