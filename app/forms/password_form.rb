class PasswordForm
  include ActiveModel::Model
  
  attr_accessor :password
  validates                 :password, presence: true
  validates_length_of       :password, within: 6..20
end
