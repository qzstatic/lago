class SubscribeForm
  include Virtus.model
  include ActiveModel::Conversion
  include ActiveModel::Validations

  attribute :email, String
  attribute :type_slug, String
  
  validates :email, presence: true, email: true
  validates :type_slug, presence: true
  
  def email=(new_email)
    super(new_email.strip.downcase)
  end
end
