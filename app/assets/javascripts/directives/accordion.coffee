angular.module 'Sea'
  .directive "accordion", ['$timeout', ($timeout) ->
    scope: true
    link: (scope, element, attrs) ->
      scope.opened = attrs.accordion == 'opened'
      scope.first = true
      scope.toggle = () ->
        height = element[0].querySelector('.b-container__title').offsetHeight + 14
        if scope.opened
          element.css(
            height: (element[0].querySelector('.b-container__wrapper').offsetHeight) + 'px'
          )
          if scope.first
            scope.first = false
            $timeout scope.toggle, 0
            return
          element.removeClass('container--opened')
          element.css(
            height: height + 'px'
          )
        else
          scope.first = false
          element.addClass('container--opened')
          element.css(
            height: (element[0].querySelector('.b-container__wrapper').offsetHeight) + 'px'
          )

        scope.opened = !scope.opened
]
