angular.module('Sea')
  .directive 'idProfileForm', ['Settings', (Settings) ->
    restrict: 'AE'
    scope: {}
    controller: ['$scope','$rootScope','$http',($scope, $rootScope, $http) ->
      $rootScope.showMessage = () ->
        $rootScope.messageShown= true
      $rootScope.accept = () ->
        document.getElementById('activeVal').value = $rootScope.profile.enabled
        document.getElementById("new_profile_form").submit()
    ]
  ]
