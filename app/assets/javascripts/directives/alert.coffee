angular.module('Sea')
  .directive 'idAlert', ['Settings', (Settings) ->
    restrict: 'AE'
    template: "<ng-transclude></ng-transclude>"
    transclude: true
    controller: ['$scope', '$interval', ($scope, $interval) ->
    ]
  ]