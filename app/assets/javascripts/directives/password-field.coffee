angular.module 'Sea'
  .directive 'passwordField', () ->
    link: (scope, element, attr) ->
      if attr.passwordField
        eye = angular.element element[0].querySelector("##{attr.passwordField}")
      else
        eye = element.find('div')
      input = element.find('input')
      if eye && input
        eye.on 'mousedown touchstart', ()->
          input[0].type = 'text'
        eye.on 'mouseup mouseleave touchend touchcancel', ()->
          input[0].type = 'password'