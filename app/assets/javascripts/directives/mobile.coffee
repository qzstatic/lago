angular.module 'Sea'
.directive 'mobileOperator', ['$location', 'Settings', 'Ga', ($location, Settings, Ga) ->
  scope: true
  controller: ['$scope', ($scope) ->
    $scope.sendCustomEvent = (event) ->
      Ga([],event)
    $scope.sendEvent = (type, operator = $scope.operator) ->
      console.log(type, operator)
      switch type
        when 'mobile'
          if operator?
            switch operator
              when 'mts'
                Ga ['/virtual/mts_page_transaction'], ['send','pageview']
                Ga ['Mobile_Subscription','MTS_Click_Subs','MTS_Subs',{'nonInteraction': true}]
              when 'megafon'
                Ga ['/virtual/megafon_page_transaction'], ['send','pageview']
                Ga ['Mobile_Subscription','Megafon_Click_Subs','Megafon_Subs',{'nonInteraction': true}]
              when 'tele2'
                Ga ['/virtual/tele2_page_transaction'], ['send','pageview']
                Ga ['Mobile_Subscription','Tele2_Click_Subs','Tele2_Subs',{'nonInteraction': true}]
              when 'beeline'
                Ga ['/virtual/beeline_page_transaction'], ['send','pageview']
                Ga ['Mobile_Subscription','Beeline_Click_Subs','Beeline_Subs',{'nonInteraction': true}]
              else
                Ga ['/virtual/wifi_page_operators'], ['send','pageview']
                Ga ['Mobile_Subscription','Wifi_Click_Subs','Wifi_Subs',{'nonInteraction': true}]
        when 'full'
          Ga ['Mobile_FullSubs_Online','Click_FullSubs','Mobile_FullSubs',{'nonInteraction': true}]
        when 'auth'
          Ga ['/virtual/page_mobile_autoriz'], ['send','pageview']
          Ga ['Mobile_Subs_Autoriz','Click_Link_Autoriz','Mobile_Autoriz',{'nonInteraction': true}]
        when 'phoneSubmit'
          Ga ['/vitrual/page_getcode'], ['send','pageview']
          Ga ['Mobile_Subs_Autoriz','Click_Button_Getcode','Button_Getcode',{'nonInteraction': true}]
        when 'codeSubmit'
          Ga ['/vitrual/page_autoriz_success'], ['send','pageview']
          Ga ['Mobile_Subs_Autoriz','Click_Button_GoRead','Button_GoRead',{'nonInteraction': true}]
  ]
  link: (scope) ->
    scope.getRedirect = (operator = scope.operator) ->
      console.log "#{Settings.hosts.metynnis}/paywall_mobile/#{scope.operator}?msisdn=#{(scope.phone.replace(/\D/g,''))}&returnUrl=#{return_url}"

      switch operator
        when 'mts'
          Ga ['/virtual/wifi_mts_page_transaction'], ['send','pageview']
          Ga ['Mobile_Subscription','Wifi_Mts_Click_Subs','Wifi_Mts_Subs',{'nonInteraction': true}]
        when 'beeline'
          Ga ['/virtual/wifi_beeline_page_transaction'], ['send','pageview']
          Ga ['send','event','Mobile_Subscription','Wifi_Beeline_Click_Subs','Wifi_Beeline_Subs',{'nonInteraction': true}]
        when 'megafon'
          Ga ['/vitrual/wifi_megafon_page_transaction'], ['send','pageview']
          Ga ['Mobile_Subscription','Wifi_Megafon_Click_Subs','Wifi_Megafon_Subs',{'nonInteraction': true}]
        when 'tele2'
          Ga ['/vitrual/wifi_tele2_page_transaction'], ['send','pageview']
          Ga ['Mobile_Subscription','Wifi_Tele2_Click_Subs','Wifi_Tele2_Subs',{'nonInteraction': true}]

      window.location.href = "#{Settings.hosts.metynnis}/paywall_mobile/#{operator}?msisdn=#{(scope.phone.replace(/\D/g,''))}&returnUrl=#{return_url}&template_id=2"
    scope.phoneFocus = (e) ->
      if (!scope.phone)
        scope.phone="+7"
        if e?.target?
          $e = angular.element e.target
          if e.target.setSelectionRange?
            len = 100
            setTimeout () ->
              e.target.setSelectionRange(len, len)
            , 100
          else
            $e.val($e.val())
      scope.phone
    scope.phoneBlur = () ->
      if scope.phone=="+7"
        scope.phone=""
      scope.phone



]
