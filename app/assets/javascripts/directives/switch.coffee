angular.module 'Sea'
.directive 'switch', () ->
  restrict: 'AE'
  replace: true
  transclude: true
  template: (element, attrs) ->
    html = ''
    html += '<span'
    html +=   ' class="switch' + (if attrs.class? then ' ' + attrs.class else  '') + '"'
    html +=   if attrs.ngModel? then ' ng-click="' + attrs.disabled + ' ? ' + attrs.ngModel + ' : ' + attrs.ngModel + '=!' + attrs.ngModel + (if attrs.ngChange? then '; ' + attrs.ngChange + '()"' else '"') else ''
    html +=   ' ng-class="{ checked:' + attrs.ngModel + ', disabled:' + attrs.disabled + ' }"'
    html +=   '>'
    html +=   '<small></small>'
    html +=   '<input type="checkbox"'
    html +=     if attrs.id? then ' id="' + attrs.id + '"' else ''
    html +=     if attrs.name? then ' name="' + attrs.name + '"' else ''
    html +=     if attrs.ngModel? then ' ng-model="' + attrs.ngModel + '"' else ''
    html +=     ' style="display:none" value="true" />'
    html +=     '<span class="switch-text">'
    html +=     if attrs.on? then '<span class="on">'+attrs.on+'</span>' else ''
    html +=     if attrs.off? then '<span class="off">'+attrs.off + '</span>' else ' '
    html += '</span>'
    html

