angular.module('Sea')
  .directive 'idRegisterForm', ['Settings', (Settings) ->
    restrict: 'AE'
    scope: {}
    templateUrl: "#{Settings.host}/nolayout/sign_up"
    controller: ['$scope', '$http', 'Settings', '$element', '$compile', ($scope, $http, Settings, $element, $compile) ->
      $scope.password_registration_form = {
        mailing_type_slugs: ['newsrelease']
      };

      $scope.submit = ($event) ->
        $event.preventDefault()
        $http.post("#{Settings.host}/nolayout/sign_up_with_password", { password_registration_form: $scope.password_registration_form }).then (content) ->
          if content.headers('registered')=='true' && fbq
            fbq('track', 'CompleteRegistration');
          $($element).html($compile(content.data)($scope))
      $scope.inputStarted = (id) ->
        el = angular.element document.getElementById(id)
        if el?
          el.parent().removeClass('field_with_errors')
          err = el.parent().next()
          if err.hasClass 'error'
            err.remove()
        0

    ]
  ]
