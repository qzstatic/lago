angular.module('Sea')
  .directive 'idLoginForm', ['Settings', (Settings) ->
    restrict: 'AE'
    scope: {}
    templateUrl: "#{Settings.host}/nolayout/sign_in"
    controller: ['$scope', '$http', '$compile', '$element', ($scope, $http, $compile, $element) ->
      $scope.submit = ($event) ->
        $event.preventDefault()
        $http.post("#{Settings.host}/nolayout/sign_in_with_password", { sign_in_form: $scope.sign_in_form })
        .then (content) ->
          $($element).html($compile(content.data)($scope))
        .catch (message) ->
          if message.headers('Location')?
            location.href = message.headers('Location')
      $scope.inputStarted = (id) ->
        el = angular.element document.getElementById(id)
        if el?
          el.parent().removeClass('field_with_errors')
          err = el.parent().next()
          if err.hasClass 'error'
            err.remove()
        0

    ]
  ]
