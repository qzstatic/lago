angular.module('Sea')
  .directive 'idPasswordResetForm', ['Settings', (Settings) ->
    restrict: 'AE'
    scope: {}
    templateUrl: "#{Settings.host}/nolayout/password_reset"
    controller: ['$scope', '$http', '$compile', '$element', ($scope, $http, $compile, $element) ->
      $scope.submit = ($event) ->
        $event.preventDefault()
        $http.post("#{Settings.host}/nolayout/password_reset", { email_form: $scope.email_form }).then (content) ->
          $($element).html($compile(content.data)($scope))
    ]
  ]