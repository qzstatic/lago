angular.module('Sea')
  .directive 'idOauthForm', ['Settings', (Settings) ->
    restrict: 'AE'
    scope: {}
    controller: ['$scope', ($scope)->
      $scope.oauth_form = {
        mailing_type_slugs: ['newsrelease']
      };
    ]
  ]
