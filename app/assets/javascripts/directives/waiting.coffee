angular.module 'Sea'
.directive 'waiting', ['$http', 'Settings', '$window', '$timeout', 'Ga', ($http, Settings, $window, $timeout, Ga) ->
  link: (scope, element, attr) ->
    scope.url = attr.url
    scope.errors = {
      0: {
        text: "Что-то пошло не так. Если у вас есть мобильная подписка <a href='#{Settings.hosts.metynnis}/id/mobile/codes/new'>восстановите доступ</a> или оформите полную подписку на «Ведомости» для доступа со всех устройств",
        showPaywall: true,
        redirectUrl: "#{Settings.hosts.metynnis}/id/mobile/#{attr.operator}/new",
        buttonText: 'Попробовать еще раз',
        hideButton: true
      },
      1: {
        text: 'Внимание! Вы указали неверный код активации подписки',
        showPaywall: false,
        buttonText: 'Попробовать еще раз',
        redirectUrl: "#{Settings.hosts.metynnis}/paywall_mobile/#{attr.operator}?returnUrl=#{return_url}&owl_state=#{attr.owlState}"
      },
      2: {
        text: 'Превышен лимит попыток для ввода кода активации. Запросите код через 20 минут',
        showPaywall: false,
        buttonText: 'Вернуться на главную',
        redirectUrl: Settings.hosts.shark
      },
      3: {
        text: 'У вас уже оформлена мобильная подписка на «Ведомости». Чтобы продолжить чтение, восстановите доступ на этом устройстве',
        showPaywall: false,
        buttonText: 'Восстановить доступ',
        redirectUrl: "#{Settings.hosts.metynnis}/id/mobile/codes/new",
        hideCancel: true

      },
      4: {
        text: 'Внимание! У вас подключена услуга «Запрет контента». Чтобы оформить мобильную подписку, свяжитесь с вашим оператором. Или прямо сейчас оформите полную подписку',
        showPaywall: true,
        buttonText: 'Восстановить доступ',
        redirectUrl: "#{Settings.hosts.metynnis}/id/mobile/codes/new",
        hideCancel: true
      },
      5: {
        text: 'Внимание! Для активации подписки вам нужно ввести номер своего телефона',
        showPaywall: false,
        showTextField: true,
        buttonText: 'Отправить'
      },
      6: {
        text: "Оператор сообщил, что на вашем счету недостаточно средств. Пополните счет и попробуйте оформить мобильную подписку <a href='#{Settings.hosts.metynnis}/id/mobile/codes/new'>еще раз</a>. Или купите полную подписку",
        showPaywall: true,
        hideButton: true
      },
      7: {
        text: 'Время сессии истекло. Попробуйте оформить подписку еще раз',
        showPaywall: false,
        buttonText: 'Попробовать еще раз',
        redirectUrl: "#{Settings.hosts.metynnis}/id/mobile/#{attr.operator}/new"
      },
      8: {
        text: 'Оператор сообщил, что вы не можете оформить мобильную подписку – ранее вы добавили ее в черный список. Свяжитесь с вашим оператором или прямо сейчас оформите полную подписку',
        showPaywall: true,
        buttonText: 'Вернуться на главную',
        redirectUrl: Settings.hosts.shark
      },
      9: {
        text: 'Оператор сообщил, что вы не можете оформить мобильную подписку. Вероятно, на вашем тарифе есть ограничения по подключению услуг. Свяжитесь с вашим оператором или прямо сейчас оформите полную подписку',
        showPaywall: true,
        hideButton: true
      },
      10: {
        text: 'Мобильная подписка на «Ведомости» недоступна, потому что в вашем браузере включен режим «Экономия трафика». Перейдите в настройки браузера, отключите его и попробуйте подписаться еще раз',
        showPaywall: false,
        redirectUrl: "#{Settings.hosts.metynnis}/id/mobile/#{attr.operator}/new",
        buttonText: 'Попробовать еще раз'
      }
    }
    scope.sendError = (errCode) ->
      Ga ['Error_Page',"Call_Error_Code#{errCode+1}","Error_Code#{errCode+1}",{'nonInteraction': true}]
    scope.counter = 0
    scope.check = () ->

      $http(
        method: 'GET'
        url: scope.url
        headers:
          'Content-Type': 'application/json'
        data: ''
      )
      .then (result) ->
        if result.data.wait? && scope.counter<20
          scope.counter++

          $timeout( () ->
            scope.check()
          , 1000
          )
        else if result.data.redirect?
          myDate = new Date()
          myDate.setMonth(myDate.getMonth() + 1)
          document.cookie = "mobile_subscriber=11;expires=#{myDate};domain=#{Settings.cookie};path=/"
          $window.location.href =result.data.redirect
        else if result.data.error?
          scope.error = scope.errors[result.data.code] || scope.errors[0]
          scope.result = true
          scope.status = 'error'
          scope.sendError result.data.code || 0
          scope.sendStat result.data.code || 0
        else
          scope.error = scope.errors[0]
          scope.result = true
          scope.status = 'error'
          scope.sendError( -1)
          scope.sendStat( -1)
#          scope.text = result.data.error
#          scope.buttonText = 'Попробовать еще раз'
      .catch (error) ->
        if error?
          scope.result = true
          scope.status = 'error'
          scope.text = error.data?.error
          scope.buttonText = 'Попробовать еще раз'
          scope.sendStat  0
    scope.check()
    scope.sendStat = (errCode) ->
      fire_event([{errCode: errCode, operator: attr.operator}], {event: 'pay_error'});
]
