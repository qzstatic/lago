angular.module('Sea')
  .directive 'idRepeatLetter', ['Settings', '$compile', (Settings, $compile) ->
    restrict: 'AE'
    scope: {
      isSended: '='
    }
    controller: ['$scope', '$http', '$compile', '$element', ($scope, $http, $compile, $element) ->
      $scope.isSended = 0
      $scope.sendLetter = ($event) ->
        $event.stopPropagation()
        $event.preventDefault()
        $http
          .post(Settings.host + '/user/repeat_email_confirm')
          .then (() -> $scope.isSended = 1), (() -> $scope.isSended = 2)
    ]
    link: (scope, element, attrs) -> $compile(element.contents())(scope.$new())
  ]
