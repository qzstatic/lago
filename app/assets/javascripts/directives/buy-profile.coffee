angular.module('Sea')
.directive 'idBuyProfile', ['Settings', (Settings) ->
  restrict: 'AE'
  scope: true
  templateUrl: "#{Settings.host}/nolayout/buy_profile"
  controller: ['$scope', '$element', '$rootScope', '$http', ($scope, $element, $rootScope, $http) ->
    #скрывает или показывает форму регистрации в зависимости от пользователя
    if $($element).find('.name').text() != ''
      $rootScope.state.buyAuth.enabled = false
      fire_event([{subscription: "step3"}], {event: 'subscription'});
    else
      fire_event([{subscription: "step2"}], {event: 'subscription'});

    $scope.user = {}
    $scope.form_enabled = true
    #переключает форму ввода телефона
    $scope.toggle_form = ->
      $scope.form_enabled = !$scope.form_enabled

    $scope.save_phone = ->
      if $scope.user.phone != ''
        $http.post("#{Settings.host}/user/phone", phone: $scope.user.phone)
        .then ->
          $scope.toggle_form()
        ,
          ->
            console.log 'номер телефона не обновлен'
      else
        console.log 'пустой номер телефона'



    $scope.initUser = (firstName, lastName, phone, email) ->
      $rootScope.state.buyAuth.fullName = "#{firstName} #{lastName}"
      $scope.user = {
        first_name: firstName
        last_name: lastName
      }
      form_enabled = !phone?
  ]
]