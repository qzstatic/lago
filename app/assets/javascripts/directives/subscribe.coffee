angular.module('Sea')
.directive 'idSubscribeForm', ['Settings', (Settings) ->
  restrict: 'AE'
  scope: {}
  templateUrl: "#{Settings.host}/nolayout/subscribe/issue"
  controller: ['$scope', '$http', '$compile', '$element', ($scope, $http, $compile, $element) ->
    $scope.submit = ($event) ->
      $event.preventDefault()
      if $scope.subscribe_form
        $scope.subscribe_form['type_slug'] = $element.find('#subscribe_form_type_slug').attr('value')
      $http.post("#{Settings.host}/nolayout/subscribe/issue", { subscribe_form: $scope.subscribe_form }).then (content) ->
        $($element).html($compile(content.data)($scope))
  ]
]
