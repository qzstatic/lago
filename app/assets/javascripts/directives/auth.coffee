angular.module('Sea')
  .directive 'idAuthForm', ['Settings', (Settings) ->
    restrict: 'AE'
    scope: {}
    templateUrl: "#{Settings.host}/nolayout/auth"
    controller: ['$scope', '$attrs', ($scope, $attrs) ->
      $scope.login_form = $attrs.tab == 'sign_in'
    ]
  ]