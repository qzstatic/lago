angular.module('Sea')
  .filter 'unsafe', ['$sce', ($sce) -> (value) -> $sce.trustAsHtml(value) ]
