#= require jquery
#= require angular
#= require ng-mask.js
angular.module('Sea', ['ngMask'])
.filter 'phoneFilter', () ->
  (data) ->
    if data?
      data.replace(/\D/g,'')


