Sea = angular.module('Sea')
Sea.requires.push('ngMask')
Sea.requires.push('checklist-model')

Sea
  .config(['$sceProvider', '$httpProvider', ($sceProvider, $httpProvider) ->
    $sceProvider.enabled(false)
    $httpProvider.defaults.withCredentials = true
  ])

  .run ['$http', 'Settings', '$location', ($http, Settings, $location) ->
    $http.get("#{Settings.host}/files").then (content) -> $('head').append(content.data)
  ]
