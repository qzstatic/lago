module Shark
  module FacebookIdConvertor
    class << self
      def old_id_for(new_id)
        connections = graph.get_connections(new_id, 'ids_for_business')
        
        old_app_info = connections.find do |data|
          data['app']['id'].to_i == Settings.facebook.old_app_id
        end
        
        old_app_info['id'].to_i unless old_app_info.nil?
      end
      
    private
      def graph
        @graph ||= Koala::Facebook::API.new(
          [Settings.facebook.app_id, Settings.facebook.app_secret].join(?|)
        )
      end
    end
  end
end
