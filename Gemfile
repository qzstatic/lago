source 'https://rubygems.org'

ruby '2.1.2'

gem 'rails', '~>4.2.3'

# Templates
gem 'oj'
gem 'slim'
gem 'uglifier', '>= 1.3.0'
gem 'sass-rails'
gem 'bootstrap-sass'
gem 'coffee-rails'
gem 'active_model_serializers'

# Server
gem 'unicorn'
gem 'rack-cors'

gem 'settings', github: 'roomink/settings'

# form classes
gem 'virtus'

# oauth
gem 'omniauth'
gem 'omniauth-vkontakte'
gem 'omniauth-facebook'
gem 'omniauth-twitter'

# social
gem 'twitter', '~> 5.13.0'
gem 'koala', '~> 1.11.0rc'
gem 'vkontakte_api', '~> 1.4.3'

# api client
gem 'faraday', '~> 0.9'
gem 'faraday_middleware', '~> 0.9.1'
gem 'faraday_middleware-parse_oj', '~> 0.3'
gem 'net-http-persistent'

gem 'redis'

# debug
gem 'awesome_pry'
gem 'newrelic_rpm'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'

  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'
  gem 'better_errors'
end

group :test do
  gem 'rspec-rails'
  gem 'faker'
  gem 'guard-rspec'
  gem 'terminal-notifier'
  gem 'terminal-notifier-guard'
  gem 'webmock'
  gem 'sinatra'
end

source 'https://rails-assets.org' do
  gem 'rails-assets-angular'
  gem 'rails-assets-angular-sanitize'
  gem 'rails-assets-jquery'
end

gem 'rails-i18n', '~> 4.0.0'
gem 'autoprefixer-rails'

gem 'compass-rails'
gem 'chunky_png'

gem 'non-stupid-digest-assets'

gem 'gelf_ext', git: 'git@github.com:vedomosti/gelf_ext.git'
