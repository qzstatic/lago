require 'rails_helper'

RSpec.describe Mobile::SubscribersController, :type => :controller do

  let(:success_access_token) { '123456789' }

  describe 'GET #new' do
    it 'return success' do
      request.headers['X-Access-Token'] = success_access_token
      get :new
      expect(response).to have_http_status(:success)
    end

    it 'return unautorized' do
      get :new
      expect(response).to have_http_status(:unauthorized)
    end
  end

  describe 'POST #create' do
    let(:success_email) { 'success@example.com'}
    it 'return success' do
      request.headers['X-Access-Token'] = success_access_token
      post :create, email: success_email
      expect(response).to have_http_status(:found)
    end
    it 'return errors' do
      request.headers['X-Access-Token'] = success_access_token
      post :create, email: Faker::Internet.email
      expect(response).to have_http_status(:success)
    end
  end
end