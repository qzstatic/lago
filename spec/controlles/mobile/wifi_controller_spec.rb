require 'rails_helper'

RSpec.describe Mobile::WifiController, :type => :controller do
  describe 'GET #new' do
    it 'return success' do
      get :new
      expect(response).to have_http_status(:success)
    end
  end

  describe 'GET #show' do
    it 'return success' do
      get :show, operator: 'mts'
      expect(response).to have_http_status(:success)
    end
  end
end