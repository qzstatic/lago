require "rails_helper"

RSpec.describe Mobile::CodesController, :type => :controller do

  let(:success_access_token) { '123456789' }
  let(:foo_access_token) { '0000000'}
  let(:success_phone){ '79999999999' }
  let(:success_code) { '11111'}


  describe 'GET #new' do
    it 'return success' do
      request.headers['X-Access-Token'] = success_access_token
      get :new
      expect(response).to have_http_status(:success)
    end
  end

  describe 'POST #create' do
    it 'return success' do
      request.headers['X-Access-Token'] = success_access_token
      post :create,  phone: success_phone
      expect(response).to have_http_status(:success)
    end
    it 'return errors' do
      request.headers['X-Access-Token'] = success_access_token
      post :create,  phone: Faker::PhoneNumber.phone_number
      expect(response).to have_http_status(:success)
    end
  end

  describe 'POST #check' do
    it 'return success' do
      request.headers['X-Access-Token'] = success_access_token
      post :check,  code: success_code
      expect(response).to have_http_status(:found)
    end
    it 'return page with errors' do
      request.headers['X-Access-Token'] = success_access_token
      post :check,  code: '123456789'
      expect(response).to have_http_status(:success)
    end
  end
end