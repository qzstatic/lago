require 'sinatra'
require 'byebug'

class FakeAquarium < Sinatra::Base

  get '/lago/mobile_users/:access_token' do
    if params[:access_token] && !params[:access_token].empty?
      content_type :json
      status 200
      {
          id: 1
      }.to_json
    else
      content_type :json
      status 401
    end
  end

  post '/lago/mobile_users/:access_token/codes' do
    content_type :json
    if authorized?(params[:access_token])
      if params[:attributes] && params[:attributes][:phone] && params[:attributes][:phone].eql?('79999999999')
        status 200
        {
            success: true
        }.to_json
      else
        status 422
        {
            success: false,
            errors: [
                'OOOPS'
            ]
        }.to_json
      end

    else
      status 401
    end
  end

  post '/lago/mobile_users/:access_token/subscribe' do
    content_type :json
    if authorized?(params[:access_token])
      if params[:attributes] && params[:attributes][:email].eql?('success@example.com')
        status 200
        {
            success: true,
            redirect: 'http://example.com'
        }.to_json
      else
        status 422
        {
            success: false,
            errors: [
                'foo email'
            ]
        }.to_json
      end
    else
      status 401
    end
  end

  post '/lago/mobile_users/:access_token/codes/check' do
    content_type :json
    if authorized?(params[:access_token])
      if params[:attributes] && params[:attributes][:code].eql?('11111')
        status 200
        {
            success: true
        }.to_json
      else
        status 422
        {
            success: false,
            errors: [
                'oooopss'
            ]
        }.to_json
      end
    else
      status 401
    end
  end

  def authorized?(access_token)
    !access_token.eql?('0000000')
  end
end
