# This file is used by Rack-based servers to start the application.

require ::File.expand_path('../config/environment', __FILE__)

map '/check_alive' do
  run -> (env) { [200, {}, []] }
end

run Rails.application
