app_root = File.expand_path('../../..', __FILE__)

worker_processes   8
preload_app        true
timeout            45
listen             9070, backlog: 4096
user               'app'
working_directory  app_root
stderr_path        'log/unicorn.stderr.log'
stdout_path        'log/unicorn.stdout.log'
