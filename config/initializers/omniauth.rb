Rails.application.config.middleware.use OmniAuth::Builder do
  provider :developer if Rails.env.development?
  provider :vkontakte, Settings.vkontakte.app_id, Settings.vkontakte.app_secret,
    {
      scope:      'email',
      display:    'popup',
      lang:       'ru',
      image_size: 'original'
    } if Settings.vkontakte?
  provider :facebook, Settings.facebook.app_id, Settings.facebook.app_secret,
    {
      scope:   'email',
      display: 'page'
    } if Settings.facebook?
  provider :twitter, Settings.twitter.app_id, Settings.twitter.app_secret,
    {
      image_size: 'bigger',
      authorize_params: {
        lang: 'ru'
      }
    } if Settings.twitter?
end

OmniAuth.config.full_host = -> (env) { 'http://' + Rack::Request.new(env).host }
