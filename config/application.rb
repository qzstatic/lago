require File.expand_path('../boot', __FILE__)

require 'action_controller/railtie'
require 'action_view/railtie'
require 'sprockets/railtie'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

Settings::Utils.environments << :staging

module Lago
  class Application < Rails::Application
    %w(lib app/validators).each do |path|
      config.autoload_paths << config.root.join(path).to_s
    end

    config.gelf_ext = Settings.graylog

    config.time_zone = 'Moscow'
    config.i18n.default_locale = :ru

    config.action_controller.asset_host = Settings.hosts.lago
    # Proc.new { |source, request| "#{request.protocol}#{request.host_with_port}" }

    config.assets.precompile += ['id.js', 'id.css']
    config.assets.precompile << /\.(?:svg|eot|woff|ttf)$/


    config.middleware.insert_before 0, "Rack::Cors" do
      allow do
        origins '*'
        resource '*', :headers => :any, :methods => [:get, :post, :options]
      end
    end

  end
end
