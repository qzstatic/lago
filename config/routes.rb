Rails.application.routes.draw do
  root 'main#index', layout: true

  get '/debug' => 'main#debug'

  # Возвращает css/js файлы, необходимые для отображения форм
  get '/files', to: 'main#files'

  # Возвращает композитную форму с вкладками авторизация/регистрация
  get '/nolayout/auth', to: 'main#auth', layout: false

  # Возвращает информацию о пользователе для buy.vedomosti.ru
  get '/nolayout/buy_profile', to: 'users#buy_profile', layout: false

  # sign up with password
  get  '/sign_up',          to: 'register#sign_up_form', as: :sign_up, layout: true
  get  '/nolayout/sign_up', to: 'register#sign_up_form', layout: false

  post '/sign_up_with_password' => 'register#sign_up_with_password',            layout: true
  post '/nolayout/sign_up_with_password' => 'register#sign_up_with_password',   layout: false

  post '/sign_up_with_oauth'      => 'register#sign_up_with_oauth'
  get  '/auth/:provider/callback' => 'register#oauth_callback', provider: /[a-z0-9_]+/, layout: true
  get  '/auth/failure', to: redirect('/')

  # sign in / out
  get  '/sign_in'  => 'session#sign_in_form',          as: :sign_in,  layout: true
  get  '/nolayout/sign_in'  => 'session#sign_in_form'
  get  '/sign_out' => 'session#sign_out',              as: :sign_out, layout: true
  get  '/nolayout/sign_out' => 'session#sign_out',     layout: false

  # sign in with password
  post '/sign_in_with_password' => 'session#sign_in_with_password'
  post '/nolayout/sign_in_with_password' => 'session#sign_in_with_password', layout: false

  # template only
  get '/blocked_user'            => 'session#blocked_user', layout: true
  get '/nolayout/blocked_user'   => 'session#blocked_user'
  get '/linked_account'          => 'session#linked_account', layout: true
  get '/nolayout/linked_account' => 'session#linked_account'

  # password reset
  get  '/password_reset' => 'register#email_form',  as: :password_reset_email_form, layout: true
  get  '/nolayout/password_reset' => 'register#email_form'

  post '/password_reset' => 'register#check_email', as: :password_reset_email_send, layout: true
  post '/nolayout/password_reset' => 'register#check_email'

  get  '/password_reset/:password_reset_token' => 'register#password_form',   password_reset_token: /[a-z0-9_]+/, as: :password_form, layout: true
  get  '/nolayout/password_reset/:password_reset_token' => 'register#password_form',   password_reset_token: /[a-z0-9_]+/

  post '/password_reset/:password_reset_token' => 'register#password_update', password_reset_token: /[a-z0-9_]+/, as: :password_send, layout: true

  # Профиль пользователя
  get  '/user/profile', to: 'users#show',   as: :users_show,   layout: true
  post '/user/profile', to: 'users#update', as: :users_update, layout: true
  post '/user/phone',   to: 'users#update_phone'
  post '/user/repeat_email_confirm', to: 'users#repeat_email_confirm', as: :users_repeat_email_confirm

  get '/includes/long_cache/user_profile/:access_token' => 'sessions#user_profile', access_token: /[a-f0-9]+/
  get '/confirm' => 'register#confirm'
  get '/social_account_link/:token' => 'register#social_account_link_confirm', token: /[a-f0-9]+/, layout: true

  # Actions для пользователей с мобильной подписки
  scope format: false do
    namespace :mobile do
      get ':operator/new' => 'mobile#new', operator: /mts|beeline|megafon|tele2|wifi/
      resources :wifi, param: :operator, only: %i(new show) do
        collection do
          get ':operator/subscribe'  => 'wifi#subscribe', operator: /mts|beeline|megafon|tele2/
          get :subscribe
        end
      end
      resources :subscribers, only: %i(create new)
      resources :codes, only: %i(new create) do
        collection do
          get :confirm
          post :check
        end
      end
    end
  end
  get '/:operator/wait/:subscription_id', to: 'mobile/pages#wait'

  # contacts
  get  '/subscribe/:slug'          => 'contacts#subscribe_form', slug: /[a-z\d_]+/, layout: true
  get  '/nolayout/subscribe/:slug' => 'contacts#subscribe_form', slug: /[a-z\d_]+/
  post '/subscribe/:slug'          => 'contacts#subscribe', as: :subscribe_send, slug: /[a-z\d_]+/, layout: true
  post '/nolayout/subscribe/:slug' => 'contacts#subscribe', slug: /[a-z\d_]+/

  post '/subscribe_ajax/:slug'     => 'contacts#subscribe_ajax', slug: /[a-z\d_]+/

  get  '/subscribe_confirm/:token' => 'contacts#subscribe_confirm', layout: true
  get  '/unsubscribe/:token'       => 'contacts#unsubscribe', layout: true

  # ниже этого места ничего не трогать!
  get '/*url', to: 'application#not_found'
end
